from django.db import models

# Create your models here.

class Mcq1(models.Model):
  question = models.CharField(max_length=150)
  opt1 = models.CharField(max_length=150)
  opt2 = models.CharField(max_length=150)
  opt3 = models.CharField(max_length=150)
  opt4 = models.CharField(max_length=150)
  ans = models.CharField(max_length=10)

  def __str__(self):
    return self.question
