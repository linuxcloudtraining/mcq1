from django.apps import AppConfig


class MultipleChoiceQuestionsConfig(AppConfig):
    name = 'multiple_choice_questions'
