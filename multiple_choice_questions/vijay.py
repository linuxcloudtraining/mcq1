from django.shortcuts import render
import os
from .models import Mcq1

# Create your views here.
data1 = Mcq1.objects.all()
count1 = Mcq1.objects.count()
context = {}

def test1(request):
  context['data1'] = data1
  return render(request, "test1.html", context)


count1 = Mcq1.objects.count()
def test2(request):
  if request.method == 'POST':
    context['data1'] = data1
#    context['user_ans'] = [ request.POST.get(str(i)) for i in range(1,count1+1) ]
    real_ans = [ i.ans for i in data1 ]
    user_ans = [ request.POST.get(str(i)) for i in range(1,count1+1) ]
    ans1 = []
    for i in range(4):
      if real_ans[i] == user_ans[i]:
        ans1.insert(i,'Your ans is right.')
      else:
        ans1.insert(i,'Your ans is wrong.')
    context['ans1'] = ans1
    return render(request, "test2.html", context)

